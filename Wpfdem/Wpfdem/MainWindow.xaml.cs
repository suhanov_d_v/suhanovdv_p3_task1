﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Wpfdem
{
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();


           
            
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e) //событие нажатия кнопки логин
        {
            var login = txtLogin.Text;
            var password = txtPass.Password;

            User user;
            if ((user = Model.Context.Get().User.FirstOrDefault(q => q.UserLogin == login && q.UserPassword == password)) != null) //проверка на совпадение в бд логина и пароля
            {
                Hide();
                new Forms.ListProduct(user).ShowDialog(); //если совпадание нашлось переход на окно с продуктами с передачей данных об авторизованном пользователе
                Show();
            }
            else
            {
                MessageBox.Show("Неправильный логин или пароль"); //если нет вывод сообщения
            }
        }

        private void btnGuest_Click(object sender, RoutedEventArgs e) //событие нажатия кнопки гость
        {
            Hide();
            new Forms.ListProduct(null).ShowDialog(); //вход без передачи данных о пользователе
            Show();
        }
    }
}
