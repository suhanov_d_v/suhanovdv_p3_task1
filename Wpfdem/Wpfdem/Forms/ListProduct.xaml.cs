﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Wpfdem.Forms
{
    public partial class ListProduct : Window
    {
        private User user;
        public ListProduct(User user)
        {
            InitializeComponent();
            this.user = user;

            if (user != null) txtUser.Content = $"{user.UserSurname} {user.UserName} {user.UserPatronymic}"; //проверка на авторизацию
            else txtUser.Content = "Гость";

            cmbSort.ItemsSource = new List<string>() { "По возрастанию", "По убыванию" }; //создание элементов для сортировки
            cmbSort.SelectedIndex = 0;
            cmbFilt.ItemsSource = new List<string>() { "Все диапазоны", "0-9,99%", "10-14,99%", "15% и более" }; //создание элементов для фильтрации
            cmbFilt.SelectedIndex = 0;

            refresh();
        }

        private void refresh() //получение данных
        {

            Model.Context.Get().Product.ToList().ForEach(p =>
            {
                try
                {
                    if(p.ProductPhotoDir != null)
                    {
                        p.ProductPhoto = File.ReadAllBytes(Directory.GetCurrentDirectory() + "/Res/" + p.ProductPhotoDir); //предположительно не работает загрузка картинок из-за кириллицы в пути
                        //p.ProductPhoto = File.ReadAllBytes(@"C:/users/суханов/Desktop/dem/Wpfdem/Wpfdem/bin/Debug/Res/D034T5.jpg");
                    }
                }
                catch
                {}
            });
            Model.Context.Get().SaveChanges();

            var data = Model.Context.Get().Product.Where(p => p.ProductName.Contains(txtSearch.Text) || 
            p.ProductDescription.Contains(txtSearch.Text) ||
            p.ProductManufacturer.Contains(txtSearch.Text)).ToList(); //поиск данных по нескольким критериям если в строку поиска введено что-то, вывод всего если ничего не введено

            var data2 = Model.Context.Get().Product.ToList().Count();
            var count = data.Count; //подсчет количества результатов
            if (cmbSort.SelectedItem != null) //проверка на выбран ли элемент сортировки
            {
                switch (cmbSort.SelectedIndex)
                {
                    case 0:
                        data = data.OrderBy(q => q.ProductCost).ToList(); //сортировка по возрастанию
                        break;
                    case 1:
                        data = data.OrderByDescending(q => q.ProductCost).ToList(); //сортировка по убыванию
                        break;
                }

                if (cmbFilt.SelectedItem != null) //проверка выбран ли элемент фильтрации
                {
                    switch (cmbFilt.SelectedIndex)
                    {
                        case 0: //все
                            break;
                        case 1: //от 0 до 10 невключительно
                            data = data.Where(q => q.ProductDiscountAmount >= 0).ToList();
                            data = data.Where(q => q.ProductDiscountAmount < 10).ToList();
                            break;
                        case 2: //от 10 до 15 невключительно
                            data = data.Where(q => q.ProductDiscountAmount >= 10).ToList();
                            data = data.Where(q => q.ProductDiscountAmount < 15).ToList();
                            break;
                        case 3: //от 15
                            data = data.Where(q => q.ProductDiscountAmount >= 15).ToList();
                            break;
                    }
                }
            }

            listViewProducts.ItemsSource = data; //вывод данных
            lblCount.Content = $"{data.Count} из {data2}"; //подсчет результатов сколько из скольки
        }

        private void btnExit_Click(object sender, RoutedEventArgs e) //кнопка выхода
        {
            Close(); //закрыть окно
        }

        private void txtSearch_TextChanged(object sender, TextChangedEventArgs e) //событие текстового поля на изменение текста
        {
            refresh(); //метод получения данных
        }

        private void cmbSort_SelectionChanged(object sender, SelectionChangedEventArgs e) //событие списка на смену элемента
        {
            refresh(); //метод получения данных
        }

        private void cmbFilt_SelectionChanged(object sender, SelectionChangedEventArgs e) //событие списка на смену элемента
        {
            refresh(); //метод получения данных
        }
    }
}
