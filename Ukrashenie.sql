USE [master]
GO
/****** Object:  Database [user19]    Script Date: 14.04.2022 12:57:13 ******/
CREATE DATABASE [user19]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'user19', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.WSRSQL\MSSQL\DATA\user19.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 1024KB )
 LOG ON 
( NAME = N'user19_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL15.WSRSQL\MSSQL\DATA\user19_log.ldf' , SIZE = 1536KB , MAXSIZE = 2048GB , FILEGROWTH = 10%)
 WITH CATALOG_COLLATION = DATABASE_DEFAULT
GO
ALTER DATABASE [user19] SET COMPATIBILITY_LEVEL = 150
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [user19].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [user19] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [user19] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [user19] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [user19] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [user19] SET ARITHABORT OFF 
GO
ALTER DATABASE [user19] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [user19] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [user19] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [user19] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [user19] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [user19] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [user19] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [user19] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [user19] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [user19] SET  ENABLE_BROKER 
GO
ALTER DATABASE [user19] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [user19] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [user19] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [user19] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [user19] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [user19] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [user19] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [user19] SET RECOVERY FULL 
GO
ALTER DATABASE [user19] SET  MULTI_USER 
GO
ALTER DATABASE [user19] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [user19] SET DB_CHAINING OFF 
GO
ALTER DATABASE [user19] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [user19] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [user19] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [user19] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
EXEC sys.sp_db_vardecimal_storage_format N'user19', N'ON'
GO
ALTER DATABASE [user19] SET QUERY_STORE = OFF
GO
USE [user19]
GO
/****** Object:  Table [dbo].[Order]    Script Date: 14.04.2022 12:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Order](
	[OrderID] [int] IDENTITY(1,1) NOT NULL,
	[OrderStatus] [nvarchar](max) NOT NULL,
	[OrderDeliveryDate] [datetime] NOT NULL,
	[OrderPickupPoint] [bigint] NOT NULL,
 CONSTRAINT [PK__Order__C3905BAF68E3DFD3] PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderPickup]    Script Date: 14.04.2022 12:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderPickup](
	[id] [bigint] NOT NULL,
	[Index] [nchar](10) NOT NULL,
	[City] [nchar](10) NOT NULL,
	[Street] [varchar](50) NOT NULL,
 CONSTRAINT [PK_OrderPickup] PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[OrderProduct]    Script Date: 14.04.2022 12:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[OrderProduct](
	[OrderID] [int] NOT NULL,
	[ProductArticleNumber] [nvarchar](100) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[OrderID] ASC,
	[ProductArticleNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Product]    Script Date: 14.04.2022 12:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Product](
	[ProductArticleNumber] [nvarchar](100) NOT NULL,
	[ProductName] [nvarchar](max) NOT NULL,
	[ProductDescription] [nvarchar](max) NOT NULL,
	[ProductCategory] [nvarchar](max) NOT NULL,
	[ProductManufacturer] [nvarchar](max) NOT NULL,
	[ProductCost] [decimal](19, 4) NOT NULL,
	[ProductDiscountAmount] [tinyint] NOT NULL,
	[ProductQuantityInStock] [int] NOT NULL,
	[ProductUnit] [nchar](10) NOT NULL,
	[ProductPhotoDir] [varchar](50) NULL,
	[ProductPhoto] [varbinary](max) NULL,
 CONSTRAINT [PK__Product__2EA7DCD55B2115FC] PRIMARY KEY CLUSTERED 
(
	[ProductArticleNumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Role]    Script Date: 14.04.2022 12:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Role](
	[RoleID] [bigint] IDENTITY(1,1) NOT NULL,
	[RoleName] [nvarchar](100) NOT NULL,
 CONSTRAINT [PK__Role__8AFACE3AC079405B] PRIMARY KEY CLUSTERED 
(
	[RoleID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[User]    Script Date: 14.04.2022 12:57:13 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[User](
	[UserID] [int] IDENTITY(1,1) NOT NULL,
	[UserSurname] [nvarchar](100) NOT NULL,
	[UserName] [nvarchar](100) NOT NULL,
	[UserPatronymic] [nvarchar](100) NOT NULL,
	[UserLogin] [nvarchar](max) NOT NULL,
	[UserPassword] [nvarchar](max) NOT NULL,
	[UserRole] [bigint] NOT NULL,
 CONSTRAINT [PK__User__1788CCAC1DBC6D65] PRIMARY KEY CLUSTERED 
(
	[UserID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[Order] ON 

INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (11, N'Новый ', CAST(N'2022-05-07T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (12, N'Новый ', CAST(N'2022-05-08T00:00:00.000' AS DateTime), 3)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (13, N'Новый ', CAST(N'2022-05-09T00:00:00.000' AS DateTime), 5)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (14, N'Завершен', CAST(N'2022-05-10T00:00:00.000' AS DateTime), 6)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (15, N'Новый ', CAST(N'2022-05-11T00:00:00.000' AS DateTime), 7)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (16, N'Новый ', CAST(N'2022-05-12T00:00:00.000' AS DateTime), 1)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (17, N'Завершен', CAST(N'2022-05-13T00:00:00.000' AS DateTime), 2)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (18, N'Новый ', CAST(N'2022-05-14T00:00:00.000' AS DateTime), 3)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (19, N'Завершен', CAST(N'2022-05-15T00:00:00.000' AS DateTime), 6)
INSERT [dbo].[Order] ([OrderID], [OrderStatus], [OrderDeliveryDate], [OrderPickupPoint]) VALUES (20, N'Новый ', CAST(N'2022-05-16T00:00:00.000' AS DateTime), 2)
SET IDENTITY_INSERT [dbo].[Order] OFF
GO
INSERT [dbo].[OrderPickup] ([id], [Index], [City], [Street]) VALUES (1, N'344556    ', N'г. Ковров ', N'Kommunist st. 10')
INSERT [dbo].[OrderPickup] ([id], [Index], [City], [Street]) VALUES (2, N'432456    ', N'г. Ковров ', N'Shosse st. 56')
INSERT [dbo].[OrderPickup] ([id], [Index], [City], [Street]) VALUES (3, N'456432    ', N'г. Ковров ', N'Sadovaya st. 66')
INSERT [dbo].[OrderPickup] ([id], [Index], [City], [Street]) VALUES (4, N'333456    ', N'г. Ковров ', N'Pobeda st. 2')
INSERT [dbo].[OrderPickup] ([id], [Index], [City], [Street]) VALUES (5, N'465522    ', N'г. Ковров ', N'Sport st. 1')
INSERT [dbo].[OrderPickup] ([id], [Index], [City], [Street]) VALUES (6, N'324567    ', N'г. Ковров ', N'Polevaya st. 13')
INSERT [dbo].[OrderPickup] ([id], [Index], [City], [Street]) VALUES (7, N'432145    ', N'г. Ковров ', N'Mayakovskogo st. 55')
GO
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'B025Y5', N'Блюдо', N'Блюдо декоративное flower 35 см Home Philosophy', N'Интерьерные аксессуары', N'Home Philosophy', CAST(1890.0000 AS Decimal(19, 4)), 3, 8, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'B037T5', N'Блюдо', N'Блюдо декоративное Flower 35 см', N'Интерьерные аксессуары', N'Home Philosophy', CAST(690.0000 AS Decimal(19, 4)), 2, 14, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'D034T5', N'Диффузор', N'Диффузор Mikado intense Апельсин с корицей', N'Ароматы для дома', N'Miksdo', CAST(800.0000 AS Decimal(19, 4)), 2, 15, N'шт.       ', N'D034T5.jpg', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'D044T5', N'Декор настенный', N'Декор настенный Фантазия 45х45х1 см', N'Интерьерные аксессуары', N'Home Philosophy', CAST(1790.0000 AS Decimal(19, 4)), 3, 7, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'D826T5', N'Диффузор', N'Диффузор True Scents 45 мл манго', N'Ароматы для дома', N'True Scents', CAST(600.0000 AS Decimal(19, 4)), 2, 13, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'D832R2', N'Растение', N'Декоративное растение 102 см', N'Интерьерные аксессуары', N'Home Philosophy', CAST(1000.0000 AS Decimal(19, 4)), 3, 15, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'D947R5', N'Диффузор', N'Диффузор Aroma Harmony Lavender', N'Ароматы для дома', N'Aroma', CAST(500.0000 AS Decimal(19, 4)), 4, 6, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'F837T5', N'Фоторамка', N'Шкатулка для украшений из дерева Stowit', N'Картины и фоторамки', N'Gallery', CAST(999.0000 AS Decimal(19, 4)), 2, 15, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'F903T5', N'Фоторамка', N'Фоторамка Gallery 20х30 см', N'Картины и фоторамки', N'Gallery', CAST(600.0000 AS Decimal(19, 4)), 2, 3, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'F928T5', N'Фоторамка', N'Фоторамка Prisma 10х10 см', N'Картины и фоторамки', N'Umbra', CAST(1590.0000 AS Decimal(19, 4)), 2, 13, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'F933T5', N'Кашпо', N'Настольное кашпо с автополивом Cube Color', N'Горшки и подставки', N'Cube Color', CAST(1400.0000 AS Decimal(19, 4)), 4, 23, N'шт.       ', N'F933T5.jpg', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'F937R4', N'Фоторамка', N'Фоторамка 10х15 см Gallery серый с патиной/золотой', N'Картины и фоторамки', N'Gallery', CAST(359.0000 AS Decimal(19, 4)), 4, 17, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'H023R8', N'Часы', N'Часы настенные Ribbon 30,5 см', N'Часы', N'Umbra', CAST(5600.0000 AS Decimal(19, 4)), 2, 6, N'шт.       ', N'H023R8.jpg', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'H937R3', N'Часы', N'Часы настенные 6500 30,2 см', N'Часы', N'Umbra', CAST(999.0000 AS Decimal(19, 4)), 3, 4, N'шт.       ', N'H937R3.jpg', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'K083T5', N'Аромат', N'Сменный аромат Figue noire 250 мл', N'Ароматы для дома', N'Esteban', CAST(2790.0000 AS Decimal(19, 4)), 2, 6, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'K478R4', N'Аромат', N'Аромат для декобукета Esteban', N'Ароматы для дома', N'Esteban', CAST(3500.0000 AS Decimal(19, 4)), 4, 4, N'шт.       ', N'K478R4.jpg', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'K937T4', N'Аромат', N'Деко-букет Кедр 250 мл', N'Ароматы для дома', N'Esteban', CAST(7900.0000 AS Decimal(19, 4)), 2, 17, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'P846R4', N'Подставка', N'Подставка для цветочных горшков 55x25x35 см Valley', N'Горшки и подставки', N'Valley', CAST(1400.0000 AS Decimal(19, 4)), 3, 12, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'P927R2', N'Подставка', N'Подставка для цветочных горшков Valley', N'Горшки и подставки', N'Valley', CAST(4000.0000 AS Decimal(19, 4)), 2, 4, N'шт.       ', N'P927R2.jpg               ', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'P936E4', N'Подставка', N'Подставка для газет и журналов Zina', N'Горшки и подставки', N'Umbra', CAST(3590.0000 AS Decimal(19, 4)), 4, 9, N'шт.       ', N'P936E4.jpg               ', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'R836R5', N'Шкатулка', N'Шкатулка для украшений из дерева Stowit', N'Шкатулки и подставки', N'Umbra', CAST(8000.0000 AS Decimal(19, 4)), 5, 3, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'R922T5', N'Шкатулка', N'Шкатулка из керамики Lana 6х7 см', N'Шкатулки и подставки', N'Home Philosophy', CAST(690.0000 AS Decimal(19, 4)), 2, 4, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'S039T5', N'Свеча', N'Свеча True Moods Feel happy', N'Свечи', N'True Scents', CAST(250.0000 AS Decimal(19, 4)), 2, 18, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'S563T4', N'Свеча', N'Свеча в стакане True Scents', N'Свечи', N'True Scents', CAST(1000.0000 AS Decimal(19, 4)), 3, 12, N'шт.       ', N'S563T4.jpg               ', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'S936Y5', N'Свеча', N'Свеча в стакане True Scents', N'Ароматы для дома', N'True Scents', CAST(299.0000 AS Decimal(19, 4)), 3, 4, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'S937T5', N'Подсвечник', N'Подсвечник 37 см', N'Ароматы для дома', N'Kersten', CAST(2600.0000 AS Decimal(19, 4)), 3, 23, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'V432R6', N'Ваза', N'Ваза из фаянса 28,00 x 9,50 x 9,50 см', N'Вазы', N'Kersten', CAST(1990.0000 AS Decimal(19, 4)), 3, 30, N'шт.       ', NULL, NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'V483R7', N'Ваза', N'Ваза «Весна» 18 см стекло, цвет прозрачный', N'Вазы', N'Весна', CAST(100.0000 AS Decimal(19, 4)), 3, 7, N'шт.       ', N'V483R7.jpg               ', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'V783T5', N'Ваза', N'Ваза из керамики Betty', N'Вазы', N'Home Philosophy', CAST(1300.0000 AS Decimal(19, 4)), 2, 13, N'шт.       ', N'V783T5.jpg               ', NULL)
INSERT [dbo].[Product] ([ProductArticleNumber], [ProductName], [ProductDescription], [ProductCategory], [ProductManufacturer], [ProductCost], [ProductDiscountAmount], [ProductQuantityInStock], [ProductUnit], [ProductPhotoDir], [ProductPhoto]) VALUES (N'V937E4', N'Ваза', N'Ваза 18H2535S 30,5 см', N'Вазы', N'Kersten', CAST(1999.0000 AS Decimal(19, 4)), 3, 21, N'шт.       ', NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Role] ON 

INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (1, N'Клиент')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (2, N'Менеджер')
INSERT [dbo].[Role] ([RoleID], [RoleName]) VALUES (3, N'Администратор')
SET IDENTITY_INSERT [dbo].[Role] OFF
GO
SET IDENTITY_INSERT [dbo].[User] ON 

INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (1, N'Алексеев ', N'Владислав', N'Аркадьевич', N'loginDEbct2018', N'Qg3gff', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (2, N'Савельева ', N'Евфросиния', N'Арсеньевна', N'loginDEvtg2018', N'ETMNzL', 3)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (3, N'Никонов ', N'Мэлс', N'Лукьевич', N'loginDEuro2018', N'a1MIcO', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (4, N'Горшкова ', N'Агафья', N'Онисимовна', N'loginDEpst2018', N'0CyGnX', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (5, N'Горбачёв ', N'Пантелеймон', N'Германович', N'loginDEpsu2018', N'Vx9cQ{', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (6, N'Ершова ', N'Иванна ', N'Максимовна', N'loginDEzqs2018', N'qM9p7i', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (7, N'Туров ', N'Денис ', N'Геласьевич', N'loginDEioe2018', N'yMPu&2', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (8, N'Носова ', N'Наина', N'Эдуардовна', N'loginDEcmk2018', N'3f+b0+', 3)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (9, N'Куликов ', N'Андрей ', N'Святославович', N'loginDEfsp2018', N'&dtlI+', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (10, N'Нестеров ', N'Агафон ', N'Георгьевич', N'loginDExcd2018', N'SZXZNL', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (11, N'Козлов ', N'Геласий ', N'Христофорович', N'loginDEvlf2018', N'O5mXc2', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (12, N'Борисова ', N'Анжелика ', N'Анатольевна', N'loginDEanv2018', N'Xiq}M3', 3)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (13, N'Суханов ', N'Станислав', N'Фролович', N'loginDEzde2018', N'tlO3x&', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (14, N'Тетерина ', N'Феврония ', N'Эдуардовна', N'loginDEriv2018', N'GJ2mHL', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (15, N'Муравьёва ', N'Александра ', N'Ростиславовна', N'loginDEhcp2018', N'n2nfRl', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (16, N'Новикова ', N'Лукия ', N'Ярославовна', N'loginDEwjv2018', N'ZfseKA', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (17, N'Агафонова ', N'Лариса', N'Михаиловна', N'loginDEiry2018', N'5zu7+}', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (18, N'Сергеева ', N'Агата ', N'Юрьевна', N'loginDEgbr2018', N'}+Ex1*', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (19, N'Колобова ', N'Елена ', N'Евгеньевна', N'loginDExxv2018', N'+daE|T', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (20, N'Ситников ', N'Николай ', N'Филатович', N'loginDEbto2018', N'b1iYMI', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (21, N'Белов ', N'Роман ', N'Иринеевич', N'loginDEfbs2018', N'v90Rep', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (22, N'Волкова ', N'Алла ', N'Лукьевна', N'loginDEple2018', N'WlW+l8', 3)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (23, N'Кудрявцева ', N'Таисия ', N'Игоревна', N'loginDEhhx2018', N'hmCHeQ', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (24, N'Семёнова ', N'Октябрина ', N'Христофоровна', N'loginDEayn2018', N'Ka2Fok', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (25, N'Смирнов ', N'Сергей ', N'Яковович', N'loginDEwld2018', N'y9HStF', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (26, N'Брагина ', N'Алина', N'Валерьевна', N'loginDEhuu2018', N'X31OEf', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (27, N'Евсеев ', N'Игорь ', N'Донатович', N'loginDEmjb2018', N'5mm{ch', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (28, N'Суворов ', N'Илья ', N'Евсеевич', N'loginDEdgp2018', N'1WfJjo', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (29, N'Котов ', N'Денис ', N'Мартынович', N'loginDEgyi2018', N'|7nYPc', 3)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (30, N'Бобылёва ', N'Юлия ', N'Егоровна', N'loginDEmvn2018', N'Mrr9e0', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (31, N'Брагин ', N'Бронислав ', N'Георгьевич', N'loginDEfoj2018', N'nhGc+D', 3)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (32, N'Александров ', N'Владимир ', N'Дамирович', N'loginDEuuo2018', N'42XmH1', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (33, N'Фокин ', N'Ириней ', N'Ростиславович', N'loginDEhsj2018', N's+jrMW', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (34, N'Воронов ', N'Митрофан', N'Антонович', N'loginDEvht2018', N'zMyS8Z', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (35, N'Маслов ', N'Мстислав ', N'Антонинович', N'loginDEeqo2018', N'l5CBqA', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (36, N'Щербаков ', N'Георгий ', N'Богданович', N'loginDExrf2018', N'mhpRIT', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (37, N'Кириллова ', N'Эмилия ', N'Федосеевна', N'loginDEfku2018', N'a1m+8c', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (38, N'Васильев ', N'Серапион ', N'Макарович', N'loginDExix2018', N'hzxtnn', 3)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (39, N'Галкина ', N'Олимпиада', N'Владленовна', N'loginDEqrf2018', N'mI8n58', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (40, N'Яковлева ', N'Ксения ', N'Онисимовна', N'loginDEhlk2018', N'g0jSed', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (41, N'Калашникова ', N'Александра', N'Владимировна', N'loginDEwoe2018', N'yOtw2F', 3)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (42, N'Медведьева ', N'Таисия ', N'Тихоновна', N'loginDExyu2018', N'7Fg}9p', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (43, N'Карпова ', N'Ольга ', N'Лукьевна', N'loginDEcor2018', N'2cIrC8', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (44, N'Герасимов ', N'Мстислав ', N'Дамирович', N'loginDEqon2018', N'YeFbh6', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (45, N'Тимофеева ', N'Ксения ', N'Валерьевна', N'loginDEyfd2018', N'8aKdb0', 1)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (46, N'Горбунов ', N'Вячеслав ', N'Станиславович', N'loginDEtto2018', N'qXYDuu', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (47, N'Кошелева ', N'Кира ', N'Владиславовна', N'loginDEdal2018', N'cJWXL0', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (48, N'Панфилова ', N'Марина ', N'Борисовна', N'loginDEbjs2018', N'Xap2ct', 2)
INSERT [dbo].[User] ([UserID], [UserSurname], [UserName], [UserPatronymic], [UserLogin], [UserPassword], [UserRole]) VALUES (49, N'Кудрявцев ', N'Матвей ', N'Игоревич', N'loginDEdof2018', N'kD|LRU', 2)
SET IDENTITY_INSERT [dbo].[User] OFF
GO
ALTER TABLE [dbo].[Order]  WITH CHECK ADD  CONSTRAINT [FK_Order_OrderPickup] FOREIGN KEY([OrderPickupPoint])
REFERENCES [dbo].[OrderPickup] ([id])
GO
ALTER TABLE [dbo].[Order] CHECK CONSTRAINT [FK_Order_OrderPickup]
GO
ALTER TABLE [dbo].[OrderProduct]  WITH CHECK ADD  CONSTRAINT [FK__OrderProd__Order__3F466844] FOREIGN KEY([OrderID])
REFERENCES [dbo].[Order] ([OrderID])
GO
ALTER TABLE [dbo].[OrderProduct] CHECK CONSTRAINT [FK__OrderProd__Order__3F466844]
GO
ALTER TABLE [dbo].[OrderProduct]  WITH CHECK ADD  CONSTRAINT [FK__OrderProd__Produ__403A8C7D] FOREIGN KEY([ProductArticleNumber])
REFERENCES [dbo].[Product] ([ProductArticleNumber])
GO
ALTER TABLE [dbo].[OrderProduct] CHECK CONSTRAINT [FK__OrderProd__Produ__403A8C7D]
GO
ALTER TABLE [dbo].[User]  WITH CHECK ADD  CONSTRAINT [FK__User__UserRole__38996AB5] FOREIGN KEY([UserRole])
REFERENCES [dbo].[Role] ([RoleID])
GO
ALTER TABLE [dbo].[User] CHECK CONSTRAINT [FK__User__UserRole__38996AB5]
GO
USE [master]
GO
ALTER DATABASE [user19] SET  READ_WRITE 
GO
